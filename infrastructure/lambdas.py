from aws_cdk import Duration

from aws_cdk.aws_lambda_python_alpha import PythonFunction
from constructs import Construct
from infrastructure.util import Lambda


def _get_function(
    scope: Construct,
    lambda_type: Lambda,
    shared_props: dict,
    timeout: Duration = Duration.seconds(60),
    memory_size: int = 128,
) -> PythonFunction:
    fn = PythonFunction(
        scope,
        f"{lambda_type.title()}Lambda",
        **shared_props,
        timeout=timeout,
        memory_size=memory_size,
    )
    return fn


def get_app_function(
    scope: Construct, lambda_type: Lambda, shared_props: dict = {}
) -> PythonFunction:
    return _get_function(scope, lambda_type, shared_props)
