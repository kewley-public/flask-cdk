import os
from enum import StrEnum
from pathlib import Path
from aws_cdk import (
    BundlingFileAccess,
    # aws_ec2 as ec2,
    aws_lambda as _lambda,
)
from aws_cdk.aws_lambda_python_alpha import BundlingOptions

# from json import loads

from constructs import Construct
from dotenv import dotenv_values


class Lambda(StrEnum):
    FLASK_APP = "flask-app"


LAMBDA_TO_RELATIVE_PATH = {
    Lambda.FLASK_APP: "app",
}


def check_required_env_vars(*argv: str) -> bool:
    missing_env_vars = [e for e in argv if e not in os.environ]
    errors = [
        ValueError(f"Environment variable {e} must be set") for e in missing_env_vars
    ]

    match len(errors):
        case 0:
            pass
        case 1:
            raise errors[0]
        case _:
            raise ExceptionGroup("Missing environment variables", errors)
    return True


def load_lambda_dotenv(env_name: str, lambda_name: Lambda):
    return dotenv_values(build_path(lambda_name) / f".env.{env_name}")


def build_path(lambda_name: Lambda) -> Path:
    pth = Path(__file__)
    while pth.parts[-1] != "flask-cdk":
        pth = pth.parent
    return pth / LAMBDA_TO_RELATIVE_PATH[lambda_name]


def get_common_lambda_props(
    scope: Construct, env_name: str, lambda_type: Lambda
) -> dict:
    # lambda_titleized = lambda_type.title()
    # VPC configs if we want
    # vpc = ec2.Vpc.from_lookup(scope, "default", vpc_name="vpc")
    # subnets = [
    #     ec2.Subnet.from_subnet_id(
    #         scope, f"PrivateSubnet{lambda_titleized}{index + 1}", subnet_id=subnet_id
    #     )
    #     for index, subnet_id in enumerate(loads(os.environ["SUBNETS"]))
    #
    # ]
    # subnet_selection = ec2.SubnetSelection(subnets=subnets)
    return dict(
        bundling=BundlingOptions(
            asset_excludes=[
                ".venv",
                "cdk.out",
                "infrastructure",
                "tests",
                "__pycache__",
                ".ruff.cache",
                ".idea",
                ".pytest_cache",
            ],
            build_args={"POETRY_VERSION": "1.6.1"},
            bundling_file_access=BundlingFileAccess.VOLUME_COPY,
        ),
        entry=str(build_path(lambda_name=lambda_type)),
        environment=load_lambda_dotenv(env_name, lambda_type),
        function_name=f"{lambda_type}-{env_name}",
        index="lambda_handler.py",
        runtime=_lambda.Runtime.PYTHON_3_11,
        # vpc=vpc,
        # vpc_subnets=subnet_selection
    )
