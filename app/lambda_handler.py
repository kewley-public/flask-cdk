import awsgi
from flask import (
    Flask,
    jsonify,
)


app = Flask(__name__)
# This doesn't work?
app.config["APPLICATION_ROOT"] = "/api/v1"


@app.route("/api/v1")
def index():
    return jsonify(status=200, message="Index!")


@app.route("/api/v1/hello")
def hello():
    return jsonify(status=200, message="Hello Flask!")


def handler(event, context):
    return awsgi.response(app, event, context)
